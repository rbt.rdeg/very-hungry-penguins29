(** Client module *)
open Client_state

(** used to know if the client wants a text or a graphic display *)
type render = Text | Graphic
(** The current render mode, GUI by default *)
let render = ref Graphic

(** represents a request, which will be sent to the server *)
type request = Msg.to_server

(** transform a render type in a string *)
let string_of_render = function Text -> "text" | Graphic -> "graphic"


(** initialisation function *)
let init_client () =
  Utils._log ("main: trying to connect to server " ^ !addr ^ " on port "
              ^ (string_of_int !port));

  Network.connect_to_server !addr !port;
  Utils._log "main: connected to the server";

  Utils._log ("main: render mode is " ^ (string_of_render !render))

(** update render *)
let update_render () = function
  | Text -> Text_renderer.update_render ()
  | Graphic -> failwith "TODO: update_render Text"

(** wait player input *)
let wait_input () = function
  | Text -> Text_renderer.wait_input ()
  | Graphic -> failwith "TODO: wait_input Graphic"

(** network main loop, wait for messages from the server *)
(* let network_loop () = *)
(*   while true do *)

(*     (\* TODO : kill thread with the gui *\) *)
(*     (\* TODO *\) *)
(*     Utils._log "network thread: coucou"; *)
(*     Thread.delay 2.; *)
(*   done *)

let load_json_file file =
  try
    Utils._log ("Attempting to load " ^ file ^ "...");
    gs := Some (new Game_state.game_state (Game_state.JSON file));
    filename := file
 with
    Failure s
  | Sys_error s -> Utils._log ("Failed to load " ^ file ^ ": " ^ s);
                   exit 1
  | _ -> Utils._log ("Failed to load " ^ file);
                   exit 1

(** main function *)
let _ =
  Unix.chdir Config.prefix;
  Random.self_init();
  Arg.parse [
    ("-p", Arg.Int (fun n -> port := n), "Numéro de port (par défaut : 10234)");
    ("-a", Arg.String (fun s -> addr := s),
     "Adresse du serveur (par défaut : localhost)");
    ("-multi", Arg.Unit (fun () -> game_mode := Multi),
     "Lancer une partie multijoueur");
    ("-no-gui", Arg.Unit (fun () -> render := Text),
     "Lancer le client en mode texte")
  ] load_json_file "Jeu des pingouins";

  Utils._log "main: initializing the client";
  (* TODO if !game_mode = Multi then *)
  (* init_client (); *)

  (* launch the network loop in a separate thread *)
  (* TODO if !game_mode = Multi then *)
  (* let th = Thread.create network_loop () in *)

  (match !render with
   | Graphic -> Gui.main()
   | Text -> failwith "TODO: text main loop");

  (* Thread.join th; *)

  ()
